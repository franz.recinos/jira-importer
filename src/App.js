import { useState, useRef } from "react";
import { parse, unparse } from "papaparse";
import { CSVLink } from "react-csv";
import "./App.css";

const jiraHeaders = [
  "Type",
  "Summary",
  "Description",
  "Assignee",
  "Team",
  "Labels",
];

function getTimeStamp() {
  const date = new Date();
  const year = date.getFullYear();
  const month = `${date.getMonth() + 1}`.padStart(2, "0");
  const day = `${date.getDate()}`.padStart(2, "0");
  return `${Date.now()}_${day}_${month}_${year}`;
}

function App() {
  const [parsedData, setParsedData] = useState([]);
  const [jiraTeamId, setJiraTeamId] = useState(null);
  const [jiraAssigneeId, setJiraAssigneeId] = useState(null);
  const [hasParsedData, setHasParsedData] = useState(false);
  const inputRef = useRef(null);

  const parseCSVData = (results) => {
    const { data: row, errors } = results;

    if (errors.length > 0) return;

    const parsedRow = [];

    // set issue type
    if (row.labels && row.labels.includes("Bug")) {
      parsedRow.push("Bug");
    } else {
      parsedRow.push("Task");
    }

    // set summary
    parsedRow.push(row.title);

    // set description
    if (row.description) {
      const cleanDesc = row.description.replace(/"/g, "");
      const description = `${cleanDesc} \n\n\n Imported from GitLab issue #${row.issueId}: ${row.url}`;
      parsedRow.push(description);
    }

    // set assignee or default
    parsedRow.push(jiraAssigneeId);

    // set team
    parsedRow.push(jiraTeamId);

    // set labels
    if (row?.labels) {
      const labels = row?.labels.replace(",", " ");
      parsedRow.push(`${labels} gitlab-import-${row.issueId}`);
    } else {
      parsedRow.push(`gitlab-import-${row.issueId}`);
    }

    // all together now
    setParsedData((state) => [...state, parsedRow]);
    setHasParsedData(true);

    return results;
  };

  const handleInputchange = (input) => {
    if (input.target.files && input.target.files[0]) {
      let reader = new FileReader();
      reader.readAsText(input.target.files[0]);
      reader.onloadend = (e) => {
        parse(e.target.result, {
          header: true,
          // preview: 5, //this is usefull when testing or limiting the amout of rows to generate
          transformHeader: (header) =>
            header
              .replace(/[^\w\s]/g, "")
              .toLowerCase()
              .replace(/[^a-zA-Z0-9]+(.)/g, (match, c) => c.toUpperCase()),
          step: async (results) => await parseCSVData(results),
        });
      };
    }
  };

  return (
    <div className="App">
      <header className="App-header">
        <img src="logo.png" alt="RenoRun" />
        <div>
          <fieldset>
            <label>Team id from Jira:</label>
            <input
              type="text"
              onChange={(e) => setJiraTeamId(e.target.value.replace(/ /g, ""))}
            />
          </fieldset>
          <fieldset>
            <label>Assignee from Jira:</label>
            <input
              type="text"
              onChange={(e) =>
                setJiraAssigneeId(e.target.value.replace(/ /g, ""))
              }
            />
          </fieldset>
          <fieldset>
            <label>CSV from GitLab:</label>
            <input
              ref={inputRef}
              type="file"
              accept=".csv"
              onChange={handleInputchange}
              disabled={!jiraTeamId || !jiraAssigneeId}
            />
          </fieldset>
          {hasParsedData && (
            <>
              <div>
                <small>A total of </small>
                <b>{`${parsedData.length}`}</b>
                <small> records were parsed</small>
              </div>
              <CSVLink
                data={unparse(parsedData)}
                headers={jiraHeaders}
                filename={`gitlab_export_${getTimeStamp()}.csv`}
                onClick={() => {
                  inputRef.current.value = "";
                  setParsedData([]);
                  setHasParsedData(false);
                }}
                target="_blank"
                rel="noreferrer"
              >
                Download CSV
              </CSVLink>
              <div>
                <a
                  href="https://renorun.atlassian.net/secure/BulkCreateSetupPage!default.jspa?externalSystem=com.atlassian.jira.plugins.jim-plugin%3AbulkCreateCsv"
                  target="_blank"
                  rel="noreferrer"
                >
                  Import to Jira
                </a>
              </div>
            </>
          )}
        </div>
      </header>
    </div>
  );
}

export default App;
