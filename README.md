# Simple CSV parser to import GitLab issue to Jira

## Export issues from GitLab

Go to your project's issues list and click on the `Export as CSV` icon

![](./src/assets/export_csv1.png)

and click the `Export issues` button, and email will be set with the CSV file attached.

![](./src/assets/export_csv2.png)

## Parse GitLab CSV

For this, you will need to know your team's Jira id as well as the id of the default assignee.

If you don't know your team id you can get it by doing a search for your team using JQL
by going here https://renorun.atlassian.net/browse/RND-460?jql= then type Team and select Team from the dropdown

![](./src/assets/search1.png)

then type `=` and type your team's name

![](./src/assets/search3.png)

then selected from the dropdown, it will show you the team id.

![](./src/assets/search4.png)

To get the assignee id, go to the user's profile page and ccopy the uuid at the url
for example in this profile url, `https://renorun.atlassian.net/jira/people/5f171d011c682d00296c0ac5`
the uuid is `5f171d011c682d00296c0ac5`.

Once you got this info, go to the importer page [here](https://franz.recinos.gitlab.io/jira-importer/), fill in the requested information and upload
the CSV from GitLab, once parsed, you can download the CSV for Jira.

## Import GitLab CSV to Jira

Go to your team's page in Jira and click create at the top, when the modal opens, there is a `Import issues` button at the top

![](./src/assets/import_csv1.png)

when clicked it will take you to the bulk import wizard

![](./src/assets/import_csv2.png)

Choose your parsed CSV, then hit next and select `R&D` as `Import to Project`, leave the rest as is

![](./src/assets/import_csv3.png)

Then mapped all columns To Jira fields

![](./src/assets/import_csv4.png)

Once the mapping is copmplete, you will be able to validate the import

![](./src/assets/import_csv5.png)

If all is good, you will see the count of issues that will be created

![](./src/assets/import_csv6.png)

you can start your import and profit! :tada: :tada: :tada:
